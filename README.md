# Resource Loader

A **Vue** plug-in for **resource fetching**.

## Features

* Uses **XMLHTTP** requests based on Javascript **Promises**
* **Single** file fetch or **multiple** resource pre-load
* **Loading screen**
* Fetch **retry** on request **timeout** after pre-load
* Optional **browser console** operation **output**

## Install

    <script type="text/javascript" src="/path/to/v-plugIn_resource_loader.js"></script>

## Basic Usage

    Vue.use(resource_loader, {
        output_requestInfo: [boolean],
        request_timeout: [number],
        urlCollections: { key: [array] ... }
    });

The `urlCollections` option is used for **pre-loading**, as it houses all **resource URLs** as _strings_ inside _arrays_. 

    <the-resource-loader>[slot]</the-resource-loader>

The component's **tag** will refactor as a `section` element, whose role is that of a **loading screen**. A **single instance** may be present in the **entirety** of the project.

    Vue.prototype.fetch_resource(resourceUrl, use_timeout = true, responseType = '');

Fetching resources is **quite accessible** through a **Vue prototype method**. A _promise_ will be returned.

    Vue.load_resources(collection = '');

Pre-loading is done by means of a **global Vue method**, using any _key_ from the `urlCollections` option.

## User Manual

[Resource Loader User Manual](https://bitbucket.org/thebitbros/resource-loader/src/master/docs/manual.md)

## Reporting Bugs

We are **not tracking** bug issues for now, but we'll do so later on.

## License

This software is distributed under the term of [GPLv3](https://bitbucket.org/thebitbros/resource-loader/src/master/docs/license.txt).