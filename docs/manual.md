# Resource Loader - User Manual

The _Resource Loader_ **Vue plug-in** does **AJAX** using the **XMLHTTP** object alongside **Promises** as the base formula to fetch a **single** resource on demand or **pre-load** resource batches when **UX** is **top priority**.

## Installation

    <script type="text/javascript" src="/path/to/v-plugIn_resource_loader.js"></script>

The **Promise** API is used throughout this plug-in, alongside other **ES6** features.

## Configuration

    Vue.use(resource_loader, {
        output_requestInfo: [boolean],
        request_timeout: [number],
        urlCollections: { key: [array] ... }
    });

The first option, `output_requestInfo`, is a boolean that controls the enabling of **browser console** operation **output**. When `true`, each fetch progress, status and results will be printed in the browser console. The default value is `false`.

Next, `request_timeout` is the number of **milliseconds** to be used by the `timeout` property in all XMLHTTP requests . We believe `15000` is more than enough by default.

And last, but not least, `urlCollections` is an object containing any number of string **URLs** inside arrays, which are intended to be **pre-loaded**. The next manual section will clarify this particular point.

## Usage

    <the-resource-loader>[slot]</the-resource-loader>

The whole plug-in consists of a **global Vue component** that exposes two main methods via it's own `beforeMount` hook. **Only a single instance** may be present in the entirety of the project.

Foremost, any **custom content** passed to the `slot` will be used inside the **loading screen**, which is rendered as a `section` DOM element. This screen will **curtain** any underlying content during pre-loading time. The default `slot` content is `<h1>Loading...</h1>`.

    Vue.prototype.fetch_resource(resourceUrl, use_timeout = true, responseType = '');

The first method to be covered (but actually second to be exposed in the aforementioned hook) is annexed to the **Vue instance prototype**, so any component can use it by means of the `this` keyword. Simply providing the complete target **resource URL** as a string to the `resourceUrl` argument is enough, however, two additional ones may be used to modify the request's parameters: `use_timeout` is a boolean  indicating whether or not should a **time-out** limit be put forth (default value is `true`), and `responseType` sets it's **response type** (default is an empty string, which equals to `'text'`).

When called, an **XMLHTTP request** object will be initialized and a **Promise** returned -- note that said promise will be `unresolved` until the **request finishes** with some _status code_!

    Vue.load_resources(collection = '');

And second (first to be exposed), this **global Vue method** will **pre-load** a collection of resources whose name is passed into the `collection` argument. An empty string or any other value will return an error.

The point of pre-loading is when **resource caching** is desired before showing any content to the end-user, ensuing:

1. **Summoning** of the loading screen

2. Calling _fetch\_resource_ for each **URL** string in the `collection` argument by iteration

3. **Starting** fetches will **increase** the component's `$data.active_fetches` **counter**; **successful**, **failed** or **timed-out** fetches will **decrease** it

4. The component will **emit** `'loading-finished'` when the counter is equal to zero, and the loading screen will be also **hidden**

5. If the component's `$data.retryList` has any timed-out fetches **listed for retry**, `$data.timeout` will be set to `0` and the **whole process** will be repeated starting from step 2 as a **background task**

An example of the pre-load feature can be studied in this project's _index.html_ file.

## Styling

As the loading screen's appearance is intended to be **fully customizable**, there is no default CSS markup included.

## Reporting Bugs

**This plug-in is provided as-is**, without any kind of support for now. Nevertheless, development continuity is guaranteed.