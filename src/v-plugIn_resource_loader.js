"use-strict";

const resource_loader = {
    install(Vue, options) {
	Vue.component('the-resource-loader', {
	    data () {
		return {
		    show_loadingScreen: false,
		    active_fetches: 0,
		    retryList: new Set(),
		    urlCollections: options.urlCollections || null,
		    request_parameters: {
			output_info: options.output_requestInfo || false,
			timeout: options.request_timeout || 15000
		    }
		};
	    },
	    beforeMount () {
		Vue.load_resources = this.load_resources;
		Vue.prototype.fetch_resource = this.fetch_resource;
		return;
	    },
	    methods: {
		console_output (type) {
		    if (this.request_parameters.output_info) {
			switch (type) {
			case 'log': console.log(arguments[1]); break;
			case 'info': console.info(arguments[1]); break;
			case 'warn': console.warn(arguments[1]); break;
			case 'error': console.error(arguments[1]); break;
			case 'time':
			    if (typeof(arguments[1]) == 'object') {
				console[(arguments[1].start ? 'time' : 'timeEnd')](arguments[1].label);
			    }
			    else { console.error('Expected argument object when outputting time.'); }
			    break;
			case 'table':
			    if (Array.isArray(arguments[1])) { console.table(arguments[1]); }
			    else { console.error('Expected argument array when outputting table.'); }
			    break;
			default: console.error('Unknown console output type.');
			}
			return true;
		    }
		    else { return false; }
		},
		fetch_resource (resourceUrl, use_timeout = true, responseType = '') {
		    
		    const resource_request = new XMLHttpRequest(),
			  results = (status, response) => {
			      return { status: status, resource: resourceUrl, payload: response || null };
			  },
			  handle_timeout = () => {
			      this.console_output('time', { start: false, label: resourceUrl });
			      this.console_output('warn', `Timed out! Resource listed for retry (${resourceUrl}).`);
			      this.active_fetches--;
			      return results('timeout');
			  },
			  track_downloadProgress = request => {

			      let percent_complete = (request.loaded / request.total) * 100;

			      this.console_output('log', `${resourceUrl} »»» ${Math.floor(percent_complete)}%`);
			      return;
			  };
		    
		    return new Promise ((resolve, reject) => {
			
			const stateChange_handler = () => {
			    if (resource_request.readyState == 2 && resource_request.status == 200) {
				this.console_output('info', `Fetching: ${resourceUrl}`);
				this.console_output('time', { start: true, label: resourceUrl });
				this.active_fetches++;
			    }
			    else if (resource_request.readyState == 4 && resource_request.status == 200) {
				this.console_output('time', { start: false, label: resourceUrl });
				this.console_output('info', `Fetch successful! (${resourceUrl})`);
				this.active_fetches--;
				resolve(results('success', resource_request.response));
			    }
			    else if (resource_request.readyState == 4 && resource_request.status == 404) {
				this.console_output('time', { start: false, label: resourceUrl });
				this.console_output('warn', `Fetch failed -- not on retry list (${resourceUrl}).`);
				this.active_fetches--;
				reject(results('fail'));
			    }
			    else { return; }  
			};
			
			resource_request.timeout = use_timeout ? this.request_parameters.timeout : 0;
			resource_request.ontimeout = () => reject(handle_timeout());
			resource_request.onprogress = track_downloadProgress;
			resource_request.onreadystatechange = stateChange_handler;
			resource_request.responseType = responseType;
			resource_request.open('GET', resourceUrl, true);
			resource_request.send();
			return;
		    });
		},
		load_resources (collection = '') {

		    const get_resources = isRetry => {			
			if (isRetry) {
			    this.console_output('info', 'Some resources pending on retry list...');
			    this.console_output('table', Array.from(this.retryList));
			    for (let resource of this.retryList) {
				this.fetch_resource(resource, false).finally(() => this.retryList.delete(resource));
			    }
			}
			else {
			    this.console_output('info', 'Getting resources...');
			    if (collection == '') {
				for (let collection  in this.urlCollections) {
				    this.urlCollections[collection].forEach(
					url => this.fetch_resource(url).then(success, query_retry).finally(check_activeFetches)
				    );
				    this.console_output('info', `Finished traversing collection "${collection}".`);
				}
			    }
			    else {
				for (let url of this.urlCollections[collection]) {
				    this.fetch_resource(url).then(success, query_retry).finally(check_activeFetches);
				}
			    }   
			}
			return;
		    },
			  success = result => { return; },
			  query_retry = result => result.status == 'timeout' ? this.retryList.add(result.resource) : false,
			  check_activeFetches = () => {
			      if (!this.active_fetches) {
				  this.$emit('loading-finished');
				  this.show_loadingScreen = false;
				  return this.retryList.size ? get_resources(true) : true;
			      }
			      else { return false; }
			  };

		    if (this.urlCollections === null) {
			this.console_output('warn', 'No resource collections declared.');
		    }
		    else if (typeof(collection) != 'string') {
			this.console_output('error', 'Argument collection must be a string either with a name or empty.');
		    }
		    else { this.show_loadingScreen = true; get_resources(false); }
		    return this.active_fetches;
		}
	    },
	    template: `<transition name="loading_screen"><section v-show="show_loadingScreen" id="loading_screen"><slot><h1>Loading...</h1></slot></section></transition>`
	});
    }
};
